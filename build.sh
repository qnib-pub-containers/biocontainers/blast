#!/bin/sh

if [[ $CI_COMMIT_TAG =~ ^[0-9\.]+$ ]]; then
    BLAST_VER=${CI_COMMIT_TAG}
else
    BLAST_VER=$(echo $CI_COMMIT_TAG |awk -F\- '{print $1}')
fi

echo $BLAST_VER
docker buildx build --platform linux/arm64 --build-arg=BLAST_VER=$BLAST_VER --load -t $IMAGE_TAG-arm64 .
docker push $IMAGE_TAG-arm64
docker buildx build --platform linux/amd64 --build-arg=BLAST_VER=$BLAST_VER --load -t $IMAGE_TAG-amd64 .
docker push $IMAGE_TAG-amd64
