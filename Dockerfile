# syntax=docker/dockerfile:1
FROM --platform=$BUILDPLATFORM ubuntu:jammy-20220531
ARG TARGETPLATFORM
ARG TARGETPLATFORM
ARG BLAST_URL=https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+
ARG BLAST_VER=2.13.0
RUN apt update \
    && apt install -y --no-install-recommends \
        wget libgomp1 \
    && rm -rf /var/lib/apt/lists/*
RUN <<EOT bash
  if [[ ${TARGETPLATFORM} == "linux/arm64" ]];then
    wget --no-check-certificate -qO - "${BLAST_URL}/${BLAST_VER}/ncbi-blast-${BLAST_VER}+-x64-arm-linux.tar.gz" \
      |tar xfz - -C /usr/local --strip-components=1
  elif [[ ${TARGETPLATFORM} == "linux/amd64" ]];then
    wget --no-check-certificate -qO - "${BLAST_URL}/${BLAST_VER}/ncbi-blast-${BLAST_VER}+-x64-linux.tar.gz" \
      |tar xfz - -C /usr/local --strip-components=1
  fi
EOT